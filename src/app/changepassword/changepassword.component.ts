import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService, AuthenticationService } from '../_services/index';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangePasswordComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
    userid: string;
    email: string;
    username: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService) { }

    ngOnInit() {
        let user = JSON.parse(localStorage.getItem('currentUser'));
        this.userid=user._id;
        this.email=user.email;
        this.username=user.username;
        
    }

    changelogin() {
        this.loading = true;
     
        // console.log("change",  this.userid)
        this.authenticationService.changepass(this.model.oldpassword, this.model.newpassword, this.userid, this.email, this.username)
        .subscribe(
            data => {
                let date = new Date();
                let user = JSON.parse(localStorage.getItem('currentUser'));
               
                localStorage.removeItem('currentUser');
                this.router.navigate(['/login']);
            },
            error => {
                this.alertService.error(error.error.message);
                this.loading = false;
            });
    }
}